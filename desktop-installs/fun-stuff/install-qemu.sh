#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install all needed packages for full support
sudo pacman -S --needed --noconfirm ebtables iptables dnsmasq vde2 virt-manager
sudo pacman -S --needed --noconfirm qemu libvirt edk2-ovmf qemu-arch-extra
sudo pacman -S --needed --noconfirm qemu-block-iscsi qemu-guest-agent

# Enable libnvirt daemon 
sudo systemctl enable libvirtd.service



