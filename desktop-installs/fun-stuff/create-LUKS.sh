#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo
echo "What drive are we encrypting (example sdb1)?"

read INPUT1

sudo cryptsetup luksFormat /dev/$INPUT1

echo
echo "What should the drive be called (example crypt01)?"

read INPUT2

sudo cryptsetup luksOpen /dev/$INPUT1 $INPUT2

sudo mkfs.ext4 /dev/mapper/$INPUT2 -L $INPUT2
