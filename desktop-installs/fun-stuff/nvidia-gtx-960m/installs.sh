#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo pacman -S --needed --noconfirm bumblebee mesa mesa-libgl nvidia primus xf86-video-intel xorg-xrandr

sudo cp xorg.conf /etc/X11/
sudo cp xorg.conf.nvidia /etc/bumblebee/
sudo cp bumblebee.conf /etc/bumblebee/

echo
echo "You should reboot now. Good luck!!!"




