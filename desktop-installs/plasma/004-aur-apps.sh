#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install Trizen from local package
sudo pacman -U ../packages/trizen-1:1.64-1-any.pkg.tar.xz

# Install octopi and package-query
trizen -S --needed --noconfirm package-query

# Install pamac
trizen -S --needed --noconfirm pamac-aur
trizen -S --needed --noconfirm pamac-tray-icon-plasma

# Apps from AUR
trizen -S --noconfirm --needed chromium-widevine
trizen -S --noconfirm --needed gitkraken
trizen -S --noconfirm --needed caffeine-ng
trizen -S --noconfirm --needed rar
trizen -S --noconfirm --needed youtube-dl-gui-git
trizen -S --noconfirm --needed tlpui-git
trizen -S --noconfirm --needed systemd-kcm
trizen -S --noconfirm --needed brave-bin
