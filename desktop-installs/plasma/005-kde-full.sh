#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo pacman -S --noconfirm --needed kde-applications
sudo pacman -S --noconfirm --needed kdeadmin
sudo pacman -S --noconfirm --needed kdebase
sudo pacman -S --noconfirm --needed kdegames
sudo pacman -S --noconfirm --needed kdegraphics
sudo pacman -S --noconfirm --needed kdemultimedia
sudo pacman -S --noconfirm --needed kdenetwork
sudo pacman -S --noconfirm --needed kdeutils
sudo pacman -S --noconfirm --needed kf5
