#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install trizen
sudo pacman -S --needed --noconfirm trizen

# Install octopi and package-query
trizen -S --needed --noconfirm package-query

# Install pamac
trizen -S --needed --noconfirm pamac-aur
trizen -S --needed --noconfirm pamac-tray-appindicator 
