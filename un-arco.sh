#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	BSD 2-Clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo "###################################"
echo "#### This must be run as root  ####"
echo "####   using su, NOT sudo     #####"
echo "###################################"
 
# Fix Skel
rm -rf /etc/skel
cp -r files/skel /etc/skel

# Fix Sudoers
mv /etc/sudoers /etc/sudoers.bak
cp files/sudoers /etc/sudoers

# Fix lsb-release
rm -rf /etc/lsb-release
cp files/lsb-release /etc/
rm -rf /etc/os-release
cp files/os-release /etc/

# Fix your user
#echo
#echo "What is your username?"
#read input1
#rm -rf /home/$input1
#mkdir /home/$input1
#cp -r /etc/skel/. /home/$input1

# Fix pacman.conf
mv /etc/pacman.conf /etc/pacman.arco
cp files/pacman.conf /etc/

# Fix environment
rm -rf /etc/environment
touch /etc/environment

# Copy desktop installs to user
echo "What is your username?"
read input1
cp -r desktop-installs /home/$input1
chown -R $input1:$input1 /home/$input1/desktop-installs

# Un-theme GRUB
sed -i 's/ArcoLinux/Arch/' /etc/default/grub
sed -i 's/GRUB_THEME/#GRUB_THEME/' /etc/default/grub
mv /boot/efi/EFI/ArcoLinux /boot/efi/EFI/ArchLinux
grub-mkconfig -o /boot/grub/grub.cfg

# Remove Arco packages (re-base)
pacman -Rcns --noconfirm arcolinuxd-system-config-git
pacman -Rcns --noconfirm arcolinux-grub-theme-vimix-git
pacman -Rcns --noconfirm arcolinux-mirrorlist-git
pacman -Rcns --noconfirm arcolinux-welcome-app-git
pacman -Rcns --noconfirm arcolinux-neofetch-git
pacman -Rcns --noconfirm arcolinux-wallpapers-git
pacman -Rcns --noconfirm arcolinux-keyring

# Remove superflous packages (Comment these out if you uses a desktop install)
pacman -Rcns --noconfirm mesa
pacman -Rcns --noconfirm xorg
pacman -Rcns --noconfirm neofetch

# Remove /usr/share stuff
rm -rf /usr/share/icons/
rm -rf /usr/share/themes/
rm -rf /usr/share/X11/
rm -rf /etc/X11/

# Clear package cache
pacman -Scc --noconfirm

# Re-populate keyring
pacman-key --init
pacman-key --populate archlinux

# Update system
pacman -Syyu
